package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.entities.Account;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class BankingSystemImpl implements BankingSystem {

    private List<Account> ACCOUNT_LIST;

    public BankingSystemImpl() {

        Account account1 = new Account("123456789", new BigDecimal("1000.0"));
        Account account2 = new Account("111111111", new BigDecimal("1000.0"));
        Account account3 = new Account("222222222", new BigDecimal("1000.0"));
        Account account4 = new Account("333333333", new BigDecimal("1000.0"));
        Account account5 = new Account("444444444", new BigDecimal("1000.0"));

        ACCOUNT_LIST = Arrays.asList(account1, account2, account3, account4, account5);
    }

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {

        Account account = getAccountByAccountNumber(accountNumber);
        return account.getAmount();
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {

        Account account = getAccountByAccountNumber(accountNumber);
        BigDecimal accountBalance = getAccountBalance(accountNumber);

        accountBalance = accountBalance.subtract(amount);
        account.setAmount(accountBalance);

    }

    private Account getAccountByAccountNumber(String accountNumber) {

        Account accountStream = ACCOUNT_LIST.stream()
                .filter(account -> account.getAccountNumber().equals(accountNumber))
                .findAny()
                .orElse(null);

        if (accountStream == null) {
            throw new AccountNotFoundException("Account Number not found!");
        }

        return accountStream;
    }

}
