package com.progressoft.induction.atm;

import com.progressoft.induction.atm.entities.BankNoteCount;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public class ATMImpl implements ATM {

    private List<BankNoteCount> BANK_NOTE_COUNT_LIST;

    private BankingSystemImpl bankingSystem = new BankingSystemImpl();

    private List<Banknote> BANK_NOTE_LIST = new ArrayList<>(EnumSet.allOf(Banknote.class));

    public ATMImpl() {
        BankNoteCount noteNumber1 = new BankNoteCount(Banknote.FIVE_JOD, 100);
        BankNoteCount noteNumber2 = new BankNoteCount(Banknote.TEN_JOD, 100);
        BankNoteCount noteNumber3 = new BankNoteCount(Banknote.TWENTY_JOD, 20);
        BankNoteCount noteNumber4 = new BankNoteCount(Banknote.FIFTY_JOD, 10);

        BANK_NOTE_COUNT_LIST = Arrays.asList(noteNumber1, noteNumber2, noteNumber3, noteNumber4);
    }

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {

        BigDecimal accountBalance = bankingSystem.getAccountBalance(accountNumber);

        if (accountBalance.compareTo(amount) == -1) {
            throw new InsufficientFundsException("Account does not have sufficient balance!");
        }

        if (amount.compareTo(getTotalBankNotesAmountAvailable()) == 1) {
            throw new NotEnoughMoneyInATMException("Not enough notes in ATM");
        }

        List<Banknote> banknoteList = getBankNoteListByAmount(amount);
        bankingSystem.debitAccount(accountNumber, amount);

        return banknoteList;
    }

    private BankNoteCount getNoteNumberByBankNote(Banknote banknote) {

        BankNoteCount bankNoteCount = BANK_NOTE_COUNT_LIST.stream()
                .filter(noteNumber -> noteNumber.getBanknote().equals(banknote))
                .findAny()
                .orElse(null);

        return bankNoteCount;

    }

    private BigDecimal getTotalBankNotesAmountAvailable() {

        BigDecimal total = BigDecimal.ZERO;

        for (Banknote banknote : BANK_NOTE_LIST) {
            BankNoteCount bankNoteCount = getNoteNumberByBankNote(banknote);

            BigDecimal count = bankNoteCount == null ? BigDecimal.ZERO : new BigDecimal(bankNoteCount.getCount());
            total = total.add(banknote.getValue().multiply(count));
        }

        return total;

    }

    private List<Banknote> getBankNoteListByAmount(BigDecimal amount) {

        List<Banknote> results = new ArrayList<>();

        /*
         * Loop until amount is zero
         */
        while (amount.compareTo(BigDecimal.ZERO) == 1) {

            for (Banknote banknote : BANK_NOTE_LIST) {
                /*
                 * get BankNoteCount object which contains Banknote count value
                 */
                BankNoteCount bankNoteCount = getNoteNumberByBankNote(banknote);
                if ((amount.compareTo(banknote.getValue()) == 1 || amount.compareTo(banknote.getValue()) == 0) && bankNoteCount.getCount() != 0) {
                    amount = amount.subtract(banknote.getValue());
                    results.add(banknote);
                    bankNoteCount.setCount(bankNoteCount.getCount() - 1);
                }
            }
        }

        return results;
    }
}
